# Administration

*- software configuration and utilization.*

## Applications & Programs

- Applications:
    - Firefox
    - Inkscape
    - [KeePassXC](apps/keepassxc.md)
    - mpv
    - Steam
- Programs:
    - Git
    - Neovim
    - ShellCheck
    - Zsh

## Operating Systems

## Services
