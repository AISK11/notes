# KeePassXC

*- password manager.*

## Configuration

- Linux: `~/.config/keepassxc/keepassxc.ini`
- Windows: `~\AppData\Roaming\KeePassXC\keepassxc.ini`
    ```conf
    [General]
    ConfigVersion=2
    NumberOfRememberedLastDatabases=10
    UpdateCheckMessageShown=true

    [GUI]
    ApplicationTheme=dark
    CheckForUpdates=false
    CompactMode=true
    HideUsernames=true
    MonospaceNotes=true
    ShowExpiredEntriesOnDatabaseUnlockOffsetDays=7

    [PasswordGenerator]
    AdvancedMode=true
    Braces=true
    Dashes=true
    Length=32
    Logograms=true
    Math=true
    Punctuation=true
    Quotes=true

    [Security]
    HideTotpPreviewPanel=true
    Security_HideNotes=true
    ```
