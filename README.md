# Notes

*- personal notes.*

## Areas

### Administration

### Cybersecurity

### Development

- Programming Languages:
    - [Haskell](dev/haskell/index.md)

### Gaming

- Steam:
    - [Dota 2](gameing/steam/dota_2/index.md)
    - [Civilization VI](gaming/steam/civilization_vi/index.md)
