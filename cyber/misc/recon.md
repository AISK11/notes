# Reconnaissance (TA0043)

*- information gathering.*

## Cheat Sheet

### Search Open Websites/Domains (T1593)

- [Google dorking](https://www.google.com) ([GHDB](https://www.exploit-db.com/google-hacking-database)):
    | Syntax                     | Example                             | Description                       |
    |:---------------------------|:------------------------------------|:----------------------------------|
    | `<text>`                   | `google`                            | Match.                            |
    | `"<text>"`                 | `"google"`                          | Match exactly (case-insensitive). |
    | `*`                        | `<text> * <text>`                   | Match any.                        |
    | `-<text>` \| `-<operator>` | `-google` \| `-ext:pdf`             | Do not match.                     |
    | `<number>..<number>`       | `1..100`                            | Match number range.               |
    | `site:<domain>`            | `site:com` \| `site:google.com`     | Match on specific website.        |
    | `inurl:<text>`             | `inurl:google`                      | Match in webpage URL.             |
    | `intitle:<text>`           | `intitle:google`                    | Match in webpage title.           |
    | `intext:<text>`            | `intext:google`                     | Match in webpage text.            |
    | `inanchor:<text>`          | `inanchor:google`                   | Match in hyperlink anchor.        |
    | `link:<text>`              | `link:google`                       | Match in hyperlink reference.     |
    | `ext:<extension>`          | `ext:pdf`                           | Match filetype.                   |
    | `after:<yy-mm-dd>`         | `after:2020-01-01`                  | Match after date.                 |
    | `before:<yy-mm-dd>`        | `before:2020-01-01`                 | Match before date.                |
    | `location:<country>`       | `location:canada` \| `location:usa` | Match origin location.            |

### Gather Victim Network Information (T1590)

Subdomains

- DNS lookup:
    ```sh
    dig <domain> <A|AAAA|CNAME|TXT> +short
    ```
- Reverse DNS lookup
    ```sh
    dig -x <ip> +short
    ```

## Resources

### Links

- [MITRE ATT&CK - Reconnaissance](https://attack.mitre.org/tactics/TA0043/)
- [OSINT Framework](https://osintframework.com/)
- [Shodan](https://www.shodan.io/) - computer search engine.

- [Netcraft](sitereport.netcraft.com)
- [](search.censys.io/search?)

### Tools

- [Nmap](https://nmap.org/download.html)
- [whois](https://github.com/rfc1036/whois/) - WHOIS client.
